#language: pt

@perfil
Funcionalidade: Alterar informações do meu perfil do Whatsapp 
para que meus contatos possam visualizar as alterações que eu realizar

Contexto: Dado que eu esteja logado no Whatsapp
E clique em Configurações

@foto
Cenário: Realizar substituição da foto do perfil
Quando eu clicar na minha foto
E clicar no botão de edição
E selecionar Galeria
E selecionar uma nova imagem
Então realizarei a alteração na minha foto do perfil

@nome
Cenário: Realizar alteração do nome do meu perfil
Quando eu clicar no botão de edição do  meu nome
E colocar o novo nome do meu perfil
Então realizarei a alteração do meu nome 


