#language: pt

@grupo
Funcionalidade: Realizar configurações no Grupo do Whatsapp
para que eu possa silenciar o grupo de acordo com minha necessidade

Contexto: Dado que eu esteja logado no Whatsapp
E clique no Grupo de Conversa

@silenciar
Cenário: Silenciar Grupo por 1 ano
Quando eu clicar no botão de Configuração 
E selecionar Silenciar
E selecionar 1 ano
Então não verei mais mensagens do Grupo por esse período

@naosilenciar
Cenário: Desmarcar a opção de Silenciar Gurpo
Quando eu clicar no botão de Configuração 
E selecionar Não Silenciar
E selecionar 1 ano
Então recebei mensagens normalmente pelo Grupo
